//now it load express module with `require` directive
var express = require('express')
var app = express()
//Define request response in root URL (/) and response with text Hello World!
app.get('/', function (req, res) {
  res.send('Este é um teste NodeJS com Docker do Paulo!')
})

var port = process.env.PORT || 3000;
//Launch listening server and consoles the log.
app.listen(port, function () {
  console.log('app listening on port %s!',port)
})
